/******************************************************************************
 * @file     ble.h
 *
 * @brief    for TLSR chips
 *
 * @author   public@telink-semi.com;
 * @date     Sep. 30, 2010
 *
 * @attention
 *
 *  Copyright (C) 2019-2020 Telink Semiconductor (Shanghai) Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *****************************************************************************/
#ifndef BLE_H_
#define BLE_H_


#include "ble_common.h"
#include "ble_format.h"

#include "controller/ble_controller.h"
#include "host/ble_host.h"

#include "hci/hci.h"
#include "hci/hci_const.h"
#include "hci/hci_cmd.h"
#include "hci/hci_event.h"

#include "service/ota/ota.h"
#include "service/ota/ota_server.h"
#include "service/device_information.h"
#include "service/hids.h"
#include "service/uuid.h"


/*********************************************************/
//Remove when file merge to SDK //

/*********************************************************/




#endif /* BLE_H_ */
